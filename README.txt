About This Library
------------------------

This library is pure java implementation of Jpeg2000 decoder and encoder with no dependency on native Java native imaging libraries.


How to use:
===========

Compile and Install
-------------------
mvn clean  -Dmaven.test.skip=true install




